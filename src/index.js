import Resolver from '@forge/resolver';
import { storage } from '@forge/api';

const resolver = new Resolver();

resolver.define('getText', async (req) => {
  console.log(req);
  const text = await storage.get('text-input');
  return text;
});

resolver.define('setText', (req) => {
  console.log(req);
  storage.set('text-input',req.payload['text-input'])
});
export const handler = resolver.getDefinitions();
