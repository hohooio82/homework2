import React, { useState, useEffect } from 'react';

import Form, { Field, FormFooter } from '@atlaskit/form';
import { invoke } from '@forge/bridge';

import Button from '@atlaskit/button/standard-button';
import TextField from '@atlaskit/textfield';



const FormField = () => {
	const [formState, setFormState] = useState('');

	const onSubmit = async (formData) => {
		setFormState(formData['text-input']);
		invoke('setText', formData);
	};

	useEffect(async () => {
		const text = await invoke('getText');
		text !== undefined ? setFormState(text) : null;
	}, [])

	return (
		<div >
			<Form onSubmit={onSubmit}>
				{({ formProps }) => (
					<form {...formProps}>
						<Field
							aria-required={true}
							name="text-input"
							defaultValue={formState}
							label="text-input"
						>
							{({ fieldProps }) => <TextField {...fieldProps} />}
						</Field>
						<FormFooter>
							<Button type='submit'>
								Submit
							</Button>
						</FormFooter>
					</form>
				)}
			</Form>
		</div>
	)
};

export default FormField